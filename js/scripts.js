$('#vinyl-round').hide();
$('#dance').hide();
$('#play-section').hide();

var totalSeconds;
var clockUpTask;
var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var hoursLabel = document.getElementById("hours");

translations = {
  es: {
    next_session: 'PRÓXIMA SESIÓN...',
    session_started: 'LA SESIÓN YA HA EMPEZADO...',
    days: 'días',
    hours: 'horas',
    minutes: 'minutos',
    seconds: 'segundos',
    press_play: 'Dale al play i se sincronizará al momento',
    thank_you: 'Gracias Tribu ! Gràcies DJ!<br><br>Ha terminado la música ...<br><br>tu danza es la que mueve la vida',
    instructions: 'Esta sesión de prueba es para probar hasta cuantas conexiones puede soportar a la vez el servidor.<br>La sesión es una antigua.<br>Puedes dejar el volumen alto o bajo, bailar o no bailar, da i gual, pero al menos estar conectad@ 30 min o una hora.<br>Muchas gracias por colaborar',
  },
  ca: {
    next_session: 'PROPERA SESSIÓ...',
    session_started: 'LA SESSIÓ JA HA COMENÇAT...',
    days: 'dies',
    hours: 'hores',
    minutes: 'minuts',
    seconds: 'segons',
    press_play: 'Apreta play i es  sincronizarà al moment',
    thank_you: 'Gràcies Tribu ! Gràcies DJ!<br><br>Ha acabat la música ... <br><br>la teva dansa és la que mou la vida',
    instructions: 'Aquesta sessió de proves és per provar quantes connexions pot soportar el servidor a la vegada.<br>La sessió és una antiga.<br>Pots deixar el volum baix o alt, ballar o no ballar, tu decidèixes, però estar connectad@ almenys 30 min o una hora. <br>Moltes gràcies per col·laborar.',
  }
}

language = $('#language').val();
fillPage(translations[language]);
checkSession();

function fillPage(translation) {
  $('#next-session').html(translation.next_session);
  $('#session-started').html(translation.session_started);
  $('#days-text').html(translation.days);
  $('#hours-text').html(translation.hours);
  $('#minutes-text').html(translation.minutes);
  $('#seconds-text').html(translation.seconds);
  $('#press-play').html(translation.press_play);
  $('#instructions').html(translation.instructions);
}

function pause() {
  var audio = document.getElementById("audio2");
  audio.pause();
  $('#vinyl-round').hide();
  $('#play-section').show();
  $('#clockdiv div span').addClass('paused');
  clearInterval(clockUpTask);
}
function play() {
  var audio = document.getElementById("audio2");
  date1 = new Date();
  date2 = new Date( start_date );
  seconds = Math.abs(date1 - date2) / 1000;
  audio.currentTime = seconds;
  audio.play();
  $('#clockdiv').show();
  $('#vinyl-round').show();
  $('#play-section').hide();
  $('#days-box').hide();
  $('#clockdiv div span').removeClass('paused');
  totalSeconds = parseInt(seconds+1);
  clockUpTask = setInterval(ClockUp, 1000);
}

// Time till next session
function getTimeRemaining(endtime) {
  const total = Date.parse(endtime) - Date.parse(new Date());
  const seconds = Math.floor((total / 1000) % 60);
  const minutes = Math.floor((total / 1000 / 60) % 60);
  const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
  const days = Math.floor(total / (1000 * 60 * 60 * 24));


  return {
    total,
    days,
    hours,
    minutes,
    seconds
  };
}

function initializeClock(id, endtime) {
  const clock = document.getElementById(id);
  const daysSpan = clock.querySelector('.days');
  const hoursSpan = clock.querySelector('.hours');
  const minutesSpan = clock.querySelector('.minutes');
  const secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    const t = getTimeRemaining(endtime);
    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      clearInterval(timeinterval);
      $('#clockdiv').hide();
      $('#play-section').show();
    }
  }
  updateClock();
  const timeinterval = setInterval(updateClock, 1000);
}

// Checks if session has finished or started
function checkSession() {
  const deadline = new Date( start_date );
  const now = new Date();
  if (now < deadline) {
    $('#session-started').hide();
    initializeClock('clockdiv', deadline);
  } else {
    date1 = new Date();
    date2 = new Date( start_date );
    seconds = Math.abs(date1 - date2) / 1000;
    if (seconds < session_duration) {
      $('#minutes').html('0');
      $('#seconds').html('0');
      $('#hours').html('00');
      $('#days').html('0');
      $('#next-session').hide();
      $('#clockdiv').hide();
      $('#play-section').show();
    } else {
      $('#next-session').hide();
      $('#clockdiv').hide();
      language = $('#language').val();
      $('#session-started').html(translations[language].thank_you)
      $('#dance').attr('style','');
      $('#dance').show();
    }
  }
}

function ClockUp() {
  if (totalSeconds > session_duration) {
    $('#next-session').hide();
    $('#clockdiv').hide();
    $('#play-section').hide();
    $('#vinyl-round').hide();
    language = $('#language').val();
    $('#session-started').html(translations[language].thank_you)
    $('#dance').attr('style','');
    $('#dance').show();
    clearInterval(clockUpTask);
  } else {
    ++totalSeconds;
    secondsLabel.innerHTML = pad(totalSeconds % 60);
    minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60) % 60);
    hoursLabel.innerHTML = pad(parseInt(totalSeconds / 60 / 60));
  }
}
function pad(val) {
  var valString = val + "";
  if (valString.length < 2) {
    return "0" + valString;
  } else {
    return valString;
  }
}

// Changes on color and languages

function bodyGreen() {
  $('body').removeClass('dark light');
}
function bodyLight() {
  $('body').removeClass('dark');
  $('body').addClass('light');
}
function bodyDark() {
  $('body').removeClass('light');
  $('body').addClass('dark');
}
function changeLanguage(new_language) {
  language = $('#language').val(new_language);
  fillPage(translations[new_language]);
  if (new_language == 'ca') {
    next_language = 'es';
  } else {
    next_language = 'ca';
  }

  $('#change-language').html(next_language.toUpperCase());
  $('#change-language').attr('onclick',"changeLanguage('"+next_language+"')");

}